#!/bin/bash

set -e
_NAME=$1
_WALLET=$2
_PLOT_SIZE=$3

if service docker status | grep -q 'Docker is not running'
then
  echo "Not running!"
  service docker start
else
  echo "Running!"
fi

echo "${_NAME}-${_WALLET}-${_PLOT_SIZE}"

sed -i 's/"--name", "INSERT_YOUR_ID"/"--name", "'${_NAME}'"/g' docker-compose.yml
sed -i 's/"--reward-address", "WALLET_ADDRESS"/"--reward-address", "'${_WALLET}'"/g' docker-compose.yml
sed -i 's/"--plot-size", "PLOT_SIZE"/"--plot-size", "'${_PLOT_SIZE}'"/g' docker-compose.yml

sudo -u "$USER" docker-compose up
