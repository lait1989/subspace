# Subspace


## Getting started

The script has some options that you can give it


- _NAME=$1 (node name) required
- _WALLET=$2 (wallet) required
- _PLOT_SIZE=$3 (plot size as 100G, 1T, 2T...) required

## Run script

sudo ./subspace.sh name wallet 100G
